export interface PostI {
    titulo: string;
    content: string;
    key?: string;
}