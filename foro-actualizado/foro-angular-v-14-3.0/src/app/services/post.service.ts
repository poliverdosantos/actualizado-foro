import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpEventType,
  HttpHeaders,
  HttpParams,
} from '@angular/common/http';
import { Post } from '../posts/Post.model';
import { map, take, tap, switchMap } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class PostService {
  constructor(private http: HttpClient, private authService: AuthService) {}

  fetchPosts() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://login-92080-default-rtdb.firebaseio.com/posts.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPost(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://login-92080-default-rtdb.firebaseio.com/posts.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Oliver',
        }),
        observe: 'body',
      }
    );
  }
  
  clearPosts() {
    this.http
      .delete('https://login-92080-default-rtdb.firebaseio.com/posts.json', {
        observe: 'events',
        responseType: 'text',
      })
      .pipe(
        tap((response) => {
          if (response.type === HttpEventType.Sent) {
            console.log('request sent');
          }

          if (response.type === HttpEventType.Response) {
            console.log(response);
          }
        })
      )
      .subscribe((response) => {
        //console.log(response);
      });
  }


  fetchPosts2() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://login-92080-default-rtdb.firebaseio.com/posts222.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPost2(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://login-92080-default-rtdb.firebaseio.com/posts222.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Oliver',
        }),
        observe: 'body',
      }
    );
  }
  


  fetchPosts3() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://login-92080-default-rtdb.firebaseio.com/posts3.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPost3(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://login-92080-default-rtdb.firebaseio.com/posts3.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Oliver',
        }),
        observe: 'body',
      }
    );
  }

/// Crear nueva publicación con el texto enriquecido

  fetchPostsNew() {
    return this.http
      .get<{ [key: string]: Post }>(
        `https://login-92080-default-rtdb.firebaseio.com/nuevaPublicacion.json`
      )
      .pipe(
        map((response) => {
          let posts: Post[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPostNew(postData: Post) {
    return this.http.post<{ name: string }>(
      'https://login-92080-default-rtdb.firebaseio.com/nuevaPublicacion.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'Nueva Publicación Oliver',
        }),
        observe: 'body',
      }
    );
  }
  

}
