import { HttpClient } from '@angular/common/http';
import { ExpansionCase } from '@angular/compiler';
import { Component, OnInit, Output, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { CategoriesComponent } from '../categories/categories.component';
import { Editor, Toolbar } from 'ngx-editor';

import { PostService } from '../services/post.service';
import { Post } from './Post.model';
import { Post2 } from './Post.model2';
import { Post3 } from './Post.module3';
import { PostNew } from './PostNew.model';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css'],
})
export class PostsComponent implements OnInit {
  postForm: FormGroup;
  postForm2: FormGroup;
  postForm3: FormGroup;
  posts: Post[];
  posts2: Post2[];
  posts3: Post3[];
  postsNew :PostNew[];
  error = null;

  html = '<b>Crea tu publicación</b><br><img src="https://eus-www.sway-cdn.com/s/wANYmrum6SVkaZXW/images/niQpVIKTwktD9k?quality=500&allowAnimation=true"   width="50%" alt="Join" title="Oliver">';

  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];

  constructor(private postService: PostService) {

    this.editor = new Editor();
  }

  ngOnInit(): void {
    this.postForm = new FormGroup({
      title: new FormControl(null, Validators.required),
      content: new FormControl(null, Validators.required),
    });
    this.postForm2 = new FormGroup({
      title2: new FormControl(null, Validators.required),
      content2: new FormControl(null, Validators.required),
    });
    this.postForm3 = new FormGroup({
      title2: new FormControl(null, Validators.required),
      content2: new FormControl(null, Validators.required),
    });

    this.getPosts();
    this.getPosts2();
    this.getPosts3();
    this.getPostsNew();
  }

  Guardar():void {
    console.log(this.html);
    alert("La informacion sera revisada por los administradores antes de ser publicada..... ❗❗❗")
    alert("Gracias por enviar tu post en un tiempo se publicara / o te daremos una respuesta....❕❕❕")
    this.html= " "
  }


  getPosts() {
    this.postService.fetchPosts().subscribe(
      (response) => {
        this.posts = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }

  onCreatePost() {
    const postData: Post = this.postForm.value;
    this.postService.createPost(postData).subscribe((response) => {
      console.log(response);
      this.getPosts();
    });
  }

  onClearPosts(event: Event) {
    event.preventDefault();
    this.postService.clearPosts();
    this.posts = [];
  }




  getPosts2() {
    this.postService.fetchPosts2().subscribe(
      (response) => {
        this.posts2 = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }

  onCreatePost2() {
    const postData: Post2 = this.postForm.value;
    this.postService.createPost2(postData).subscribe((response) => {
      console.log(response);
      this.getPosts2();
    });
  }




  getPosts3() {
    this.postService.fetchPosts3().subscribe(
      (response) => {
        this.posts3 = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }

  onCreatePost3() {
    const postData: Post3 = this.postForm.value;
    this.postService.createPost3(postData).subscribe((response) => {
      console.log(response);
      this.getPosts3();

    });
  }

  

  getPostsNew() {
    this.postService.fetchPostsNew().subscribe(
      (response) => {
        this.postsNew = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }

  onCreatePostNew() {
    const postData: Post = this.postForm.value;
    this.postService.createPostNew(postData).subscribe((response) => {
      console.log(response);
      this.getPostsNew();

    });
  }
}






