
export interface PostNew {
  title: string;
  content: string;
  key?: string;
}
